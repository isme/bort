# boRt

Soluciones para el curso de R enseñado por el singular Carlos Bort.

## Clase 0

¿Qué es R?, ejercicios básicos.

[Ver los ejercicios de clase 1](MC0_student.R)

rpubs.com/Carlos_Bort

datacamp.com/groups/netquest

[RugBcn, Barcelona R user group](https://www.meetup.com/es-ES/RugBcn-Barcelona-R-users-group/)

## Clase 1

Conceptos a revisar:

* Standard deviation
* Correlation, variance, covariance
* pairwise.complete.obs?
* linear models, regression, single stratum analysis of variance, analysis of covariance
* plot linear model in R
* R^2 of a model
* Accessing attributes of a summary in R
* FactoMineR - principal component analysis?
* cor(df) - pa q sirve
* compute a hierarchical clustering?
* scale function? dist function?
